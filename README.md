# 00 Pilot Gem

00 Pilot is designed to provide an ORM like experience for ruby users of the [00-api](https://gitlab.com/frame-00/00-api). Pilot gem leverages the root api url and OPTIONs calls to automatically generate a client so you do not need to deploy need code whenever you're schemas change. 00 lazy loads all resources so that they happen at the last possible moment and caches them in many cases to avoid reloading them.

## Getting Started

Supports Rubt 2.7.x

```
gem install https://gitlab.com/frame-00/00-pilot-gem.git
```

## TODO add rails hooks here

Since Pilot Snake is made as a generalized client you will likely want to create file or package for use/reuse. Here is an example of how you might do this

```
require 'zerozero_pilot'

# TODO: update to correct urls
API_URL = ENV['API_URL']
API_TOKEN = ENV['API_TOKEN']

ZeroZeroPilot::Client.new.build(API_URL, API_TOKEN, __namespace__)
```

## Relationship to API

If you were to build a client for an API with these models in a test_app app then the client module/package will have a test_app module and inside it there will be an Example and ExamplesChild class.

```
from django.db import models

from zerozero.registry import register


class Example(models.Model):
    char = models.CharField(max_length=10)


register(Example, {"exclude": ["excluded_field"]})


class ExamplesChild(models.Model):
    parent = models.ForeignKey(
        Example,
        on_delete=models.CASCADE,
        related_name="children",
        related_query_name="children",
    )


register(ExamplesChild)
```

```
require 'zerozero_pilot'

ZeroZeroPilot::Client.new.build(API_URL, API_TOKEN, 'MY_NAMESPACE')

Example = MY_NAMESPACE::Test_App::Example
ExamplesChild = MY_NAMESPACE::Test_App::ExamplesChild

```

Example and ExamplesChild each represent a single model and all for the creation of Example and ExamplesChild objects

### Model

```
example = Example.create(char: "test")

example = Example.get(123)
examples_after_123 = Examples.list(where: { "pk__gte": 123 })

examples = Example.list()
example = examples[0]
puts example.char

example.char = 'test'
example.save()

```

#### Model with related model

```
children = ExamplesChild.list(where: {"parent__char": "test" })
child = children[0]
puts (child.parent.char)
example = Example.get(1)
example.parent = example
example.save()
```

#### Extra Info

Models have a metadata property wit the results of the OPTIONs call for each resource. This should document the schema in a quick way from the command line.

## Contributing

### Getting Started

00 Pilot should work on ruby 2 and is tested on 2.7. If anyone wants to update our tests to pass for 2.x please do so and we will update tox.

```
gem build zerozero_pilot.gemspec
gem install ./zerozero_pilot-<version>.gem
```

### Testing

All testing has been done with minitest. Tests can be run using:

```
bundle install

./run_tests.sh
```
