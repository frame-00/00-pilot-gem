# frozen_string_literal: true

require File.expand_path('lib/zerozero_pilot/version', __dir__)

Gem::Specification.new do |spec|
  spec.name        = 'zerozero_pilot'
  spec.version     = ZeroZeroPilot::VERSION
  spec.summary     = 'Ruby client application for 00-api'
  spec.description = 'Ruby client to dynamically build resources used to call external apis from 00-api'
  spec.authors     = ['Max Bolotin']
  spec.email       = 'elcommie@gmail.com'
  spec.files       = Dir['lib/**/*.rb']
  spec.homepage    = 'https://gitlab.com/frame-00/00-pilot-gem'
  spec.license     = 'Nonstandard'

  spec.required_ruby_version = '~> 2.7.x'
  spec.add_dependency 'rest-client', '~> 2.1'

  spec.add_development_dependency 'byebug', '~> 11.1.3'
  spec.add_development_dependency 'minitest', '~> 5.15'
  spec.add_development_dependency 'minitest-reporters', '~> 1.5'
  spec.add_development_dependency 'rubocop', '~> 1.25.1'
  spec.metadata['rubygems_mfa_required'] = 'true'
end
