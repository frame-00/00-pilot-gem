# frozen_string_literal: true

require_relative 'request'
require_relative 'module_utils'
require_relative 'metadata'
require_relative 'exceptions'
require_relative 'model_collection'

# Generic model base class that dynamically creates the class
module ZeroZeroPilot
  class Model
    MODEL_URL = 'NOT_DEFINED'
    TOKEN = 'NOT_DEFINED'

    def initialize(data)
      @url = data['url']
      @loaded = @url && data.keys.length > 1
      @data = data.clone
    end

    def fetch
      @data = Request.execute('GET', @url, self.class.const_get('TOKEN'))
      @loaded = true
    end

    def create
      @data = Request.execute('POST', self.class.const_get('MODEL_URL'), self.class.const_get('TOKEN'), @data)
      @loaded = true
      @url = @data['url']
    end

    def delete
      Request.execute('DELETE', @url, self.class.const_get('TOKEN'))
    end

    def save
      if !@url
        create

      elsif !@loaded
        raise ClientException, "Can't save unloaded instance"

      else
        @data = Request.execute('PUT', @url, self.class.const_get('TOKEN'), @data)
      end
    end

    # rubocop:disable Style/MissingRespondToMissing
    def method_missing(method_name, *args, &block)
      self.class.metadata

      if respond_to?(method_name)
        send(method_name, *args, &block)

      else
        super
      end
    end
    # rubocop:enable Style/MissingRespondToMissing

    class << self
      include Metadata

      def list(page_index: nil, page_size: 10, where: nil, order: nil, &block)
        query = generate_query(where, order)
        page = page_index || 1

        collection = ModelCollection.new(self, { page: page, page_size: page_size, query: query })

        if block_given?
          collection.each(&block)
        else
          collection
        end
      end

      def get(id)
        data_url = File.join(const_get('MODEL_URL'), id.to_s, '/')
        new({ 'url' => data_url })
      end

      def create(data)
        new(data).tap(&:create)
      end

      def create_from_instance(instance, column_map = {}, omissions = [])
        fields = metadata_fields
        field_data = fields.each_with_object({}) do |field, data|
          next data if omissions.include?(field)

          set_field(data, field, instance, column_map)
        end
        new(field_data)
      end

      private

      def set_field(data, field, instance, column_map)
        custom_value = column_map[field]
        return set_mapped_field(data, field, instance, custom_value) if custom_value

        data[field] = instance[field] if instance.respond_to?(field)
      end

      def set_mapped_field(data, field, instance, custom_value)
        value = custom_value.is_a?(Symbol) ? instance[custom_value] : custom_value

        data[field] = value
      end

      def generate_query(where, order)
        {}.tap do |query|
          query['where'] = where if where
          query['order'] = order if order
        end
      end

      def fetch_items(params)
        Request.execute('GET', const_get('MODEL_URL'), const_get('TOKEN'), params)
      end

      def fetch_metadata
        Request.execute('OPTIONS', const_get('MODEL_URL'), const_get('TOKEN'))
      end
    end
  end
end
