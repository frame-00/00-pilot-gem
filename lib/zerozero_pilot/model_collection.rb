# frozen_string_literal: true

# Collection class for enumerating models
class ModelCollection
  include Enumerable

  # Retrieves metadata from the zero zero server for a given model
  #
  # @param model [Model subclass]
  def initialize(model, params)
    @model = model
    @params = params.dup
    @params[:page] ||= 1
  end

  def each
    loop do
      result_set = @model.send(:fetch_items, @params)
      rows = result_set['results']

      rows.each { |row| yield @model.new(row) }

      break unless result_set['next']

      @params[:page] += 1
    end
  end
end
