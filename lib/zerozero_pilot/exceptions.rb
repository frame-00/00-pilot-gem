# frozen_string_literal: true

module ZeroZeroPilot
  # For Errors made on the client side
  class ClientException < StandardError; end

  # For Errors made when calling the zero zero server
  class ApiException < StandardError
    attr_reader :status

    def initialize(status)
      super
      @status = status
    end
  end

  # For Errors made via invalid field usage (future use)
  class ValidationException < StandardError; end

  # For Errors made trying to update read-only fields
  class ReadOnlyException < StandardError; end
end
