# frozen_string_literal: true

require_relative 'model_utils'

module ZeroZeroPilot
  VALID_OPTIONS = %i[column_map omissions on_commit].freeze
  OPERATION_SYMBOL = :OPERATION

  module ApiPersistable
    # rubocop:disable Metrics/MethodLength, Metrics/AbcSize, Metrics/PerceivedComplexity, Metrics/CyclomaticComplexity
    def zerozero_model(model_or_model_name, **args)
      ZeroZeroPilot::ModelUtils.validate_arguments(model_or_model_name, args)

      options = {
        model: model_or_model_name,
        column_map: args[:column_map] || {},
        omissions: args[:omissions]&.map(&:to_s) || [],
      }

      define_method :zerozero_create do
        model, column_map, omissions = ::ZeroZeroPilot::ModelUtils.extract_options(self, options, 'CREATE')
        model.create_from_instance(self, column_map, omissions).create
      end

      define_method :zerozero_update do
        model, column_map, omissions = ::ZeroZeroPilot::ModelUtils.extract_options(self, options, 'UPDATE')
        model.create_from_instance(self, column_map, omissions).save
      end

      define_method :zerozero_destroy do
        model, column_map, omissions = ::ZeroZeroPilot::ModelUtils.extract_options(self, options, 'DESTROY')
        model.create_from_instance(self, column_map, omissions).delete
      end

      return unless args[:on_commit]

      after_commit do
        if transaction_include_any_action?([:create])
          zerozero_create
        elsif transaction_include_any_action?([:update])
          zerozero_update
        elsif transaction_include_any_action?([:delete])
          zerozero_delete
        end
      end
    end
  end
  # rubocop:enable Metrics/MethodLength, Metrics/AbcSize, Metrics/PerceivedComplexity, Metrics/CyclomaticComplexity
end

ActiveSupport.on_load(:active_record) do
  extend ZeroZeroPilot::ApiPersistable
end
