# frozen_string_literal: true

module ZeroZeroPilot
  module ModelUtils
    def self.extract_options(instance, options, operation)
      model = constantize_or_build_model(options[:model])
      column_map = configure_map(instance, options[:column_map], operation)

      [model, column_map, options[:omissions]]
    end

    def self.constantize_or_build_model(model_or_model_name)
      model = model_or_model_name
      if model.is_a?(String)
        ZeroZeroPilot.load
        model = ZeroZeroPilot::ModuleUtils.get_constant(model)
        raise ArgumentError, "#{model_or_model_name} is not a valid api model" unless model
      end

      validate_arguments(model, {})
      model
    end

    def self.configure_map(instance, column_map, operation)
      return column_map if column_map.empty?

      new_map = get_map_by_type(instance, column_map, operation)

      # substitute operation symbol for operation being performed
      new_map = new_map.transform_keys(&:to_s)
      new_map.transform_values { |v| v == ZeroZeroPilot::OPERATION_SYMBOL ? operation : v }
    end

    def self.get_map_by_type(instance, column_map, operation)
      if column_map.is_a?(Hash)
        column_map

      elsif column_map.is_a?(Symbol)
        return instance.send(column_map) if instance.method(column_map).arity.zero?

        instance.send(column_map, operation)

      elsif column_map.respond_to?('call')
        column_map.call(operation)
      end
    end

    # rubocop:disable Style/IfUnlessModifier, Style/GuardClause
    def self.validate_arguments(model, options)
      unless model.is_a?(String) || model < ZeroZeroPilot::Model
        raise ArgumentError, "#{model} is not a valid api model"
      end

      unless (ZeroZeroPilot::VALID_OPTIONS | options.keys) == ZeroZeroPilot::VALID_OPTIONS
        raise ArgumentError, "only (#{VALID_OPTIONS.join(', ')}) are valid parameters"
      end

      validate_options(options) unless options.empty?
    end

    def self.validate_options(options)
      column_map = options[:column_map]
      unless !column_map || [Hash, Symbol].include?(column_map.class) || column_map.respond_to?(:call)
        raise ArgumentError, 'column_map must be a hash or callable'
      end

      omissions = options[:omissions]
      unless !omissions || omissions.is_a?(Array)
        raise ArgumentError, 'omissions must be an array'
      end
    end
    # rubocop:enable Style/IfUnlessModifier, Style/GuardClause
  end
end
