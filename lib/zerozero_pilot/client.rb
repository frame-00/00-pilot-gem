# frozen_string_literal: true

require_relative 'request'
require_relative 'model'
require_relative 'module_utils'

module ZeroZeroPilot
  class Client
    # Calls the zerozero server to get metadata for each available model.  Then
    # builds stub classes for each model
    #
    # @param api_url [string] the url of the api for the zero zero server
    #   (Ex. https://domain/zerozero/api)
    # @param token [string] the token used to authenticate to the zero zero
    #   server
    # @param module_name [string] the root module namespace where all metadata
    #   classes will be created
    # @raise [ClientException] if the token isn't specified
    # @raise [ApiException] if unable to retrieve metadata from the zero zero
    #   server
    def build(api_url, token, module_name = 'ZeroZeroPilot')
      models = fetch_models(api_url, token)
      base_module = ModuleUtils.constantize_module(module_name)

      models.each do |identifier, url|
        ModuleUtils.create_module_model(identifier, url, base_module, token)
      end
    end

    private

    def fetch_models(api_url, token)
      Request.execute('GET', api_url, token)
    end
  end
end
