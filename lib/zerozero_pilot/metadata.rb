# frozen_string_literal: true

require_relative 'module_utils'

module ZeroZeroPilot
  # Mixin for getting and setting metadata fields on the model class
  module Metadata
    # Retrieves metadata from the zero zero server for a given model
    #
    # @return [Metadata]
    def metadata
      @metadata ||= retrieve_metadata
    end

    # Retrieves a list of fields for the given model from the metadata
    def metadata_fields
      metadata
      @fields
    end

    private

    def retrieve_metadata
      metadata = fetch_metadata
      handle_fields(metadata['fields'])
      metadata
    end

    def handle_fields(fields)
      @fields = []
      fields.each do |name, info|
        @fields.push(name)
        related_model = get_related_model(name, info)
        create_getter(name, related_model)
        create_setter(name, info['read_only'])
      end
    end

    def create_getter(name, related_model)
      if related_model
        define_method name.intern do
          related_model.new({ 'url' => @data[name] })
        end
      else
        define_method name.intern do
          @data[name]
        end
      end
    end

    def create_setter(name, read_only)
      if read_only
        define_method "#{name}=".intern do |_arg|
          raise ReadOnlyException, 'This field is read only'
        end
      else
        define_method "#{name}=".intern do |arg|
          @data[name] = arg
        end
      end
    end

    def get_related_model(name, info)
      related_identifier = info['related_name']
      return if name == 'url' || !related_identifier

      base_module = grand_parent_module # We assume base module is 2 back in hierarchy
      related_url = info['related_url']
      related_model =
        ModuleUtils.create_module_model(related_identifier, related_url, base_module, const_get('TOKEN'))
      related_models[name] = related_model
    end

    def grand_parent_module
      Object.const_get(name.split('::')[0...-2].join)
    end

    def related_models
      @related_models ||= {}
    end
  end
end
