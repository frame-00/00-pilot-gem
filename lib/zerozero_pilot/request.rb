# frozen_string_literal: true

require 'json'
require 'rest-client'
require 'byebug'
require_relative 'exceptions'

# A request module for
module ZeroZeroPilot
  module Request
    # rubocop:disable Metrics/abcSize
    def self.execute(method, url, token, payload = nil)
      params = generate_params(method, url, token, payload)

      begin
        # TODO: use rails logger if available
        puts "00 api request: #{method} #{url}, payload: #{params[:payload]}"
        response = RestClient::Request.execute(params)
        puts "00 api response: #{response.code} #{JSON.parse(response.body)}"

        JSON.parse(response.body)
      rescue RestClient::ExceptionWithResponse => e
        raise ApiException.new(e.response.code), JSON.parse(e.response.body)
      end
    end
    # rubocop:enable Metrics/abcSize

    private_class_method def self.generate_params(method, url, token, payload)
      raise ClientException, 'Need an authentication token to use api' unless token

      headers = { Authorization: "TOKEN #{token}", 'Content-Type': 'application/json' }

      params = { headers: headers, method: method, url: url }
      params[:payload] = payload.to_json if payload

      params
    end
  end
end
