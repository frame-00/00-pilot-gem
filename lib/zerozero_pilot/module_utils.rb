# frozen_string_literal: true

module ZeroZeroPilot
  module ModuleUtils
    def self.get_constant(name, parent = Object)
      parent.const_get(name, false)
    rescue NameError
      nil
    end

    def self.constantize_module(name, parent = Object)
      constant = capitalize_constant(name)
      get_constant(constant, parent) || parent.const_set(constant, Module.new)
    end

    def self.create_module_model(identifier, url, base_module, token)
      app_name, model_name = identifier.split('.')
      app_module = constantize_module(app_name, base_module)
      create_model(model_name, app_module, url, token)
    end

    def self.create_class(url, token)
      Class.new(Model).tap do |klass|
        klass.const_set('MODEL_URL', url)
        klass.const_set('TOKEN', token)
      end
    end

    def self.create_model(name, parent_module, url, token)
      constant = capitalize_constant(name)
      klass = get_constant(constant, parent_module)
      return klass if klass

      klass = create_class(url, token)
      parent_module.const_set(constant, klass)
    end

    def self.clear_models(parent_module)
      parent_module.constants.each do |const_name|
        const = parent_module.const_get(const_name)
        if const < Model
          parent_module.send(:remove_const, const_name)
        elsif const.is_a?(Module)
          clear_models(const)
        end
      end
    end

    def self.capitalize_constant(name)
      # Upcase first letter
      name.sub(/^[a-z\d]*/, &:capitalize)
    end
  end
end
