# frozen_string_literal: true

require_relative 'zerozero_pilot/module_utils'

module ZeroZeroPilot
  class << self
    attr_accessor :api_token, :api_url, :eager_load, :namespace
    attr_reader :loaded

    def configure
      @loaded = false

      yield self

      load if eager_load
    end

    def load
      return if loaded

      Client.new.build(@api_url, @api_token, @namespace)

      @loaded = true
    end

    def reload
      ModuleUtils.clear_models(ModuleUtils.get_constant(@namespace))

      @loaded = false
      load
    end
  end

  autoload :Client, 'zerozero_pilot/client'
  autoload :Model, 'zerozero_pilot/model'
  autoload :Request, 'zerozero_pilot/request'

  autoload :ClientException, 'zerozero_pilot/exceptions'
  autoload :ApiException, 'zerozero_pilot/exceptions'
  autoload :ValidationException, 'zerozero_pilot/exceptions'
  autoload :ReadOnlyException, 'zerozero_pilot/exceptions'
end

require 'zerozero_pilot/rails/api_persistable' if defined?(Rails)
