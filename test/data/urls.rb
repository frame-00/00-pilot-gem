# frozen_string_literal: true

module TestUrls
  ROOT = 'http://test/zerozero/api/'
  EXAMPLE_LIST = "#{ROOT}test_app.Example/"
  EXAMPLE_DETAIL = "#{EXAMPLE_LIST}1/"
  EXAMPLES_CHILD_LIST = "#{ROOT}test_app.ExamplesChild/"
  EXAMPLES_CHILD_DETAIL = "#{EXAMPLES_CHILD_LIST}1/"
end
