# frozen_string_literal: true

require 'json'
require_relative 'urls'

module TestRecordsets
  Response = Struct.new(:code, :body)

  EXAMPLE = {
    'url' => TestUrls::EXAMPLE_DETAIL,
    'char' => 'bar',
  }.freeze

  EXAMPLE_LIST = {
    'next' => true,
    'results' => [
      EXAMPLE,
      {
        'url' => TestUrls::EXAMPLE_DETAIL.gsub('1', '2'),
        'char' => 'foo',
      }
    ],
  }.freeze

  EXAMPLES_CHILD = {
    'url' => TestUrls::EXAMPLES_CHILD_DETAIL,
    'parent' => TestUrls::EXAMPLE_DETAIL,
  }.freeze

  API_RESULTS = Response.new 200, {
    'test_app.Example': TestUrls::EXAMPLE_LIST,
    'test_app.ExamplesChild': TestUrls::EXAMPLES_CHILD_LIST,
  }.to_json

  EXAMPLE_RESULTS = Response.new 200, EXAMPLE.to_json

  EXAMPLE_OPTIONS_RESULTS = Response.new 200, {
    'name' => 'test_app.Example',
    'url' => TestUrls::EXAMPLE_LIST,
    'description' => '',
    'renders' => ['application/json', 'text/html', 'text/csv'],
    'parses' => [
      'application/json',
      'application/x-www-form-urlencoded',
      'multipart/form-data'
    ],
    'actions' => {
      'POST' => {
        'url' => {
          'type' => 'field',
          'required' => false,
          'read_only' => true,
          'label' => 'Url',
        },
        'char' => {
          'type' => 'string',
          'required' => false,
          'read_only' => false,
          'label' => 'Char',
          'max_length' => 10,
        },
      },
    },
    'fields' => {
      'url' => {
        'name' => 'url',
        'type' => 'relation',
        'json_type' => 'string',
        'required' => false,
        'read_only' => true,
        'related_url' => TestUrls::EXAMPLE_LIST,
        'related_name' => 'test_app.Example',
      },
      'char' => {
        'name' => 'char',
        'type' => 'text',
        'json_type' => 'string',
        'required' => true,
        'read_only' => false,
      },
    },
  }.to_json

  EXAMPLES_CHILD_OPTIONS_RESULTS = Response.new 200, {
    'name' => 'test_app.ExamplesChild',
    'url' => TestUrls::EXAMPLES_CHILD_LIST,
    'description' => '',
    'renders' => ['application/json', 'text/html', 'text/csv'],
    'parses' => [
      'application/json',
      'application/x-www-form-urlencoded',
      'multipart/form-data'
    ],
    'actions' => {
      'POST' => {
        'url' => {
          'type' => 'field',
          'required' => false,
          'read_only' => true,
          'label' => 'Url',
        },
        'parent' => {
          'type' => 'field',
          'required' => true,
          'read_only' => false,
          'label' => 'Parent',
        },
      },
    },
    'fields' => {
      'url' => {
        'name' => 'url',
        'type' => 'relation',
        'json_type' => 'string',
        'required' => false,
        'read_only' => true,
        'related_url' => TestUrls::EXAMPLES_CHILD_LIST,
        'related_name' => 'test_app.ExamplesChild',
      },
      'parent' => {
        'name' => 'parent',
        'type' => 'relation',
        'json_type' => 'string',
        'required' => true,
        'read_only' => false,
        'related_url' => TestUrls::EXAMPLE_LIST,
        'related_model' => 'Example',
        'related_name' => 'test_app.Example',
      },
    },
  }.to_json
end
