# frozen_string_literal: true

require 'rest-client'

require_relative '../data/urls'
require_relative '../data/recordsets'

module MockApis
  HEADERS = { Authorization: 'TOKEN test_auth_token', 'Content-Type': 'application/json' }.freeze

  def self.mock_rest_exception(text)
    response = TestRecordsets::Response.new 401, text.to_json
    RestClient::Unauthorized.new response
  end

  def self.mock_api_call(
    method: 'GET',
    url: TestUrls::EXAMPLE_DETAIL,
    params: nil,
    results: TestRecordsets::EXAMPLE_RESULTS,
    &block
  )
    full_params = { headers: HEADERS, method: method, url: url }
    full_params[:payload] = params if params

    with_mocked_rest_client({ params: full_params, results: results }, &block)
  end

  def self.mock_metadata_call(url = nil, results = nil, &block)
    params = { headers: HEADERS, method: 'OPTIONS', url: url || TestUrls::EXAMPLE_LIST }
    return_value = results || TestRecordsets::EXAMPLE_OPTIONS_RESULTS

    with_mocked_rest_client({ params: params, results: return_value }, &block)
  end

  def self.mock_list_call(url = nil, mocked_calls, &block)
    params = { headers: HEADERS, method: 'GET', url: url || TestUrls::EXAMPLE_LIST }
    full_calls = mocked_calls.map do |call|
      {
        results: TestRecordsets::Response.new(200, call[:results].to_json),
        params: params.merge(payload: call[:params]),
      }
    end

    with_mocked_rest_client(*full_calls, &block)
  end

  def self.mock_exception(exception, &block)
    exception_block = proc do
      raise exception
    end

    RestClient::Request.stub :execute, exception_block, &block
  end

  def self.with_mocked_rest_client(*calls, &block)
    rest_mock = MiniTest::Mock.new
    calls.each do |call|
      rest_mock.expect(:call, call[:results], [call[:params]])
    end

    RestClient::Request.stub :execute, rest_mock, &block

    rest_mock.verify
  end
end
