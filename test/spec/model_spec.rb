# frozen_string_literal: true

require 'test_helper'

describe ZeroZeroPilot::Model do
  it 'should get metadata when user calls attribute method' do
    test_module = module_with_model

    MockApis.mock_metadata_call do
      example = test_module::Test_app::Example.new(TestRecordsets::EXAMPLE)
      refute_includes example.methods, :char
      refute_includes example.methods, :url

      assert_equal 'bar', example.char
      assert_equal TestUrls::EXAMPLE_DETAIL, example.url
    end
  end

  it 'should throw an exception when user attempts setting a readonly field' do
    test_module = module_with_model

    MockApis.mock_metadata_call do
      example = test_module::Test_app::Example.new(TestRecordsets::EXAMPLE.dup)

      assert_equal 'bar', example.char
      example.char = 'foo'
      assert_equal 'foo', example.char

      returned_exception = assert_raises(ZeroZeroPilot::ReadOnlyException) { example.url = 'http://data/' }
      assert_equal returned_exception.message, 'This field is read only'
    end
  end

  it 'handles foreign keys by creating object relationships' do
    test_module = module_with_model
    create_model_for_module(test_module, 'ExamplesChild', TestUrls::EXAMPLES_CHILD_LIST)

    MockApis.mock_metadata_call(TestUrls::EXAMPLES_CHILD_LIST, TestRecordsets::EXAMPLES_CHILD_OPTIONS_RESULTS) do
      examples_child = test_module::Test_app::ExamplesChild.new(TestRecordsets::EXAMPLES_CHILD)
      example = examples_child.parent

      assert_equal "#{test_module.name}::Test_app::Example", example.class.name
      assert_equal TestUrls::EXAMPLE_DETAIL, example.instance_variable_get('@url')
    end
  end

  it 'makes calls in batches when enumerating a list' do
    test_module = module_with_model

    mocked_calls = [
      {
        params: { page: 1, page_size: 2, query: {} }.to_json,
        results: TestRecordsets::EXAMPLE_LIST,
      },
      {
        params: { 'page' => 2, 'page_size' => 2, 'query' => {} }.to_json,
        results: {
          'next' => false,
          'results' => [{
            url: TestUrls::EXAMPLE_DETAIL.gsub('1', '3'),
            char: 'bob',
          }],
        },
      }
    ]

    MockApis.mock_list_call(TestUrls::EXAMPLE_LIST, mocked_calls) do
      examples = []
      test_module::Test_app::Example.list(page_size: 2) do |example|
        examples.push(example)
      end

      assert_equal 3, examples.length
      assert_equal [test_module::Test_app::Example], examples.map(&:class).uniq
      data_sets = examples.map { |e| e.instance_variable_get('@data') }
      assert_equal [
        'http://test/zerozero/api/test_app.Example/1/',
        'http://test/zerozero/api/test_app.Example/2/',
        'http://test/zerozero/api/test_app.Example/3/'
      ], (data_sets.map { |d| d['url'] })
      assert_equal %w[bar foo bob], (data_sets.map { |d| d['char'] })
    end
  end

  it 'allows enumerating list to just fetch subset' do
    test_module = module_with_model

    # Note that results tells enumerator that there's another subset but it never gets called
    # because the take only needs 2
    results = { 'next' => true, 'results' => TestRecordsets::EXAMPLE_LIST['results'].dup }
    results['results'].push({ url: TestUrls::EXAMPLE_DETAIL.gsub('1', '3'), char: 'bob' })

    mocked_calls = [{ params: { page: 1, page_size: 2, query: {} }.to_json, results: results }]

    MockApis.mock_list_call(TestUrls::EXAMPLE_LIST, mocked_calls) do
      examples = test_module::Test_app::Example.list(page_size: 2).take(2)

      assert_equal [test_module::Test_app::Example], examples.map(&:class).uniq
      assert_equal 2, examples.length

      data_sets = examples.map { |e| e.instance_variable_get('@data') }
      assert_equal [
        'http://test/zerozero/api/test_app.Example/1/',
        'http://test/zerozero/api/test_app.Example/2/'
      ], (data_sets.map { |d| d['url'] })
      assert_equal %w[bar foo], (data_sets.map { |d| d['char'] })
    end
  end

  it 'should return unevaluated object for :get' do
    test_module = module_with_model

    example = test_module::Test_app::Example.get(1)
    refute_includes example.methods, :char
    refute_includes example.methods, :url

    assert_equal TestUrls::EXAMPLE_DETAIL, example.instance_variable_get('@url')
  end

  it 'should make post call on create' do
    test_module = module_with_model

    MockApis.mock_api_call(method: 'POST', url: TestUrls::EXAMPLE_LIST, params: TestRecordsets::EXAMPLE.to_json) do
      example = test_module::Test_app::Example.create(TestRecordsets::EXAMPLE)

      assert_equal true, example.instance_variable_get('@loaded')
    end
  end

  it 'should make get call on fetch' do
    test_module = module_with_model

    MockApis.mock_api_call do
      example = test_module::Test_app::Example.new({ 'url' => TestUrls::EXAMPLE_DETAIL })
      data = example.instance_variable_get('@data')

      refute_equal true, example.instance_variable_get('@loaded')
      assert_nil data['char']

      example.fetch

      new_data = example.instance_variable_get('@data')
      assert_equal true, example.instance_variable_get('@loaded')
      assert_equal 'bar', new_data['char']
    end
  end

  it 'should make DELETE call on delete' do
    test_module = module_with_model

    MockApis.mock_api_call(method: 'DELETE') do
      example = test_module::Test_app::Example.new({ 'url' => TestUrls::EXAMPLE_DETAIL })
      refute_equal true, example.instance_variable_get('@loaded')

      example.delete

      refute_equal true, example.instance_variable_get('@loaded')
    end
  end

  describe ':save' do
    it 'should create new if no url' do
      test_module = module_with_model

      data = { char: 'bob ' }

      MockApis.mock_api_call(method: 'POST', url: TestUrls::EXAMPLE_LIST, params: data.to_json) do
        example = test_module::Test_app::Example.new(data)
        refute_equal true, example.instance_variable_get('@loaded')
        assert_nil example.instance_variable_get('@url')

        example.save

        assert_equal TestUrls::EXAMPLE_DETAIL, example.instance_variable_get('@url')
      end
    end

    it 'should update if url and loaded' do
      test_module = module_with_model

      data = TestRecordsets::EXAMPLE

      MockApis.mock_api_call(method: 'PUT', url: TestUrls::EXAMPLE_DETAIL, params: data.to_json) do
        example = test_module::Test_app::Example.new(data)
        assert_equal true, example.instance_variable_get('@loaded')

        example.save

        assert_equal TestUrls::EXAMPLE_DETAIL, example.instance_variable_get('@url')
      end
    end

    it 'should throw an exception if changes but no data' do
      test_module = module_with_model

      example = test_module::Test_app::Example.new({ 'url' => TestUrls::EXAMPLE_DETAIL })
      refute_equal true, example.instance_variable_get('@loaded')

      returned_exception = assert_raises(ZeroZeroPilot::ClientException) { example.save }
      assert_equal returned_exception.message, "Can't save unloaded instance"
    end
  end
end
