# frozen_string_literal: true

require 'test_helper'

describe ZeroZeroPilot do
  it 'allows user to set attributes and eager load during configuration' do
    MockApis.mock_api_call(url: 'some_url', results: TestRecordsets::API_RESULTS) do
      ZeroZeroPilot.configure do |config|
        config.api_url = 'some_url'
        config.api_token = TestConstants::TOKEN
        config.namespace = 'TestModuleA'
        config.eager_load = true
      end

      assert_equal TestModuleA::Test_app.constants, %i[Example ExamplesChild]
    end
  end

  it 'Does not load data if already loaded' do
    ZeroZeroPilot.configure do |config|
      config.api_url = 'some_other_url'
      config.api_token = TestConstants::TOKEN
      config.namespace = 'TestModuleB'
      config.eager_load = false
    end

    MockApis.mock_api_call(url: 'some_other_url', results: TestRecordsets::API_RESULTS) do
      ZeroZeroPilot.load # First time makes a call

      assert_equal TestModuleB::Test_app.constants, %i[Example ExamplesChild]
    end

    ZeroZeroPilot.load # This one does not make a call
  end

  it 'Reloads models on reload even if already loaded' do
    reloaded_results = TestRecordsets::Response.new 200, {
      'test_app.DifferentExample': TestUrls::EXAMPLE_LIST,
    }.to_json

    ZeroZeroPilot.configure do |config|
      config.api_url = 'some_other_url'
      config.api_token = TestConstants::TOKEN
      config.namespace = 'TestModuleC'
      config.eager_load = false
    end

    MockApis.mock_api_call(url: 'some_other_url', results: TestRecordsets::API_RESULTS) do
      ZeroZeroPilot.load

      assert_equal TestModuleC::Test_app.constants, %i[Example ExamplesChild]
    end

    MockApis.mock_api_call(url: 'some_other_url', results: reloaded_results) do
      ZeroZeroPilot.reload

      assert_equal TestModuleC::Test_app.constants, %i[DifferentExample]
    end
  end
end
