# frozen_string_literal: true

require 'test_helper'

describe ZeroZeroPilot::Client do
  before do
    @client = ZeroZeroPilot::Client.new
  end

  def run_build
    @client.build(TestUrls::ROOT, TestConstants::TOKEN, 'Test')
  end

  it 'creates stub classes from api metadata' do
    MockApis.mock_api_call(url: TestUrls::ROOT, results: TestRecordsets::API_RESULTS) do
      run_build

      assert_equal Test::Test_app.constants, %i[Example ExamplesChild]

      # Should not have instance methods yet
      refute_includes Test::Test_app::Example.instance_methods(false), :char
      refute_includes Test::Test_app::ExamplesChild.instance_methods(false), :parent
    end
  end

  it 'throws exception if missing token' do
    returned_exception = assert_raises(ZeroZeroPilot::ClientException) do
      @client.build(TestUrls::ROOT, nil, 'Test')
    end
    assert_equal returned_exception.message, 'Need an authentication token to use api'
  end

  it 'throws exception on failed authentication' do
    exception = MockApis.mock_rest_exception('Bad Auth')

    MockApis.mock_exception(exception) do
      returned_exception = assert_raises(ZeroZeroPilot::ApiException) { run_build }
      assert_equal returned_exception.message, 'Bad Auth'
      assert_equal returned_exception.status, 401
    end
  end
end
