# frozen_string_literal: true

require 'zerozero_pilot/module_utils'

# Fake ActiveRecord::Base class
class Base
  def self.after_commit(&block)
    callbacks.push(block)
  end

  def self.callbacks
    @callbacks ||= []
  end

  def [](method)
    public_send(method)
  end

  class << self
    attr_accessor :artifact
  end
end

module ActiveSupport
  def self.on_load(artifact, &block)
    Base.class_eval(&block)
    Base.artifact = artifact
  end
end

require 'zerozero_pilot/rails/api_persistable'

class ARModel < Base
  attr_accessor :id, :char, :some_url
end

require 'test_helper'

describe ZeroZeroPilot::ApiPersistable do
  it 'should attach api persistable to active record base class' do
    assert_includes ARModel.methods, :zerozero_model
    assert_equal :active_record, Base.artifact
  end

  it 'should not have callbacks if class does not specify zero zero model' do
    assert_equal 0, ARModel.callbacks.length
  end

  it 'should create callback if on_commit specified' do
    test_module = module_with_model

    klass = Class.new(ARModel) do
      zerozero_model test_module::Test_app::Example, on_commit: true
    end

    assert_equal 1, klass.callbacks.length
  end

  it 'should create methods for crud operations' do
    test_module = module_with_model

    klass = Class.new(ARModel) do
      zerozero_model test_module::Test_app::Example, column_map: { url: :some_url }.freeze
    end

    instance = klass.new
    instance.id = 3
    instance.char = 'value'
    instance.some_url = 'http://some_url'

    # Get metadata first for client model
    MockApis.mock_metadata_call do
      test_module::Test_app::Example.new(TestRecordsets::EXAMPLE).char
    end

    payload = { 'url' => 'http://some_url', 'char' => 'value' }.to_json
    MockApis.mock_api_call(method: 'POST', url: TestUrls::EXAMPLE_LIST, params: payload) do
      instance.zerozero_create
    end

    MockApis.mock_api_call(method: 'PUT', url: 'http://some_url', params: payload) do
      instance.zerozero_update
    end

    MockApis.mock_api_call(method: 'DELETE', url: 'http://some_url') do
      instance.zerozero_destroy
    end
  end

  it 'should allow column_map to be a function' do
    test_module = module_with_model

    klass = Class.new(ARModel) do
      zerozero_model test_module::Test_app::Example, column_map: :map_char_to_op

      def map_char_to_op(operation)
        { 'url' => 'http://new_url', char: operation }
      end
    end

    instance = klass.new
    instance.id = 3
    instance.char = 'value'
    instance.some_url = 'http://some_url'

    # Get metadata first for client model
    MockApis.mock_metadata_call do
      test_module::Test_app::Example.new(TestRecordsets::EXAMPLE).char
    end

    payload = { 'url' => 'http://new_url', 'char' => 'CREATE' }.to_json
    MockApis.mock_api_call(method: 'POST', url: TestUrls::EXAMPLE_LIST, params: payload) do
      instance.zerozero_create
    end

    payload = { 'url' => 'http://new_url', 'char' => 'UPDATE' }.to_json
    MockApis.mock_api_call(method: 'PUT', url: 'http://new_url', params: payload) do
      instance.zerozero_update
    end
  end

  describe 'validations' do
    it 'should throw an exception if model does not inherit from proxy model class' do
      returned_exception = assert_raises(ArgumentError) do
        Class.new(ARModel) do
          zerozero_model Hash
        end
      end
      assert_equal returned_exception.message, 'Hash is not a valid api model'
    end

    it 'should throw an exception if options has invalid parameters' do
      returned_exception = assert_raises(ArgumentError) do
        Class.new(ARModel) do
          zerozero_model 'Some::Class', omissions: [:field1], extensions: ['trees']
        end
      end
      assert_equal returned_exception.message, 'only (column_map, omissions, on_commit) are valid parameters'
    end

    it 'should throw an exception if options is not a hash' do
      returned_exception = assert_raises(ArgumentError) do
        Class.new(ARModel) do
          zerozero_model 'Some::Class', 'extra param'
        end
      end
      assert_equal returned_exception.message, 'wrong number of arguments (given 2, expected 1)'
    end

    it 'should throw an exception if column_map is not a hash or callable' do
      returned_exception = assert_raises(ArgumentError) do
        Class.new(ARModel) do
          zerozero_model 'Some::Class', column_map: %i[field1 field2]
        end
      end
      assert_equal returned_exception.message, 'column_map must be a hash or callable'
    end

    it 'should throw an exception if omissions is not an array' do
      returned_exception = assert_raises(ArgumentError) do
        Class.new(ARModel) do
          zerozero_model 'Some::Class', omissions: { field1: 'some value ' }
        end
      end
      assert_equal returned_exception.message, 'omissions must be an array'
    end
  end
end
