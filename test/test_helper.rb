# frozen_string_literal: true

require 'byebug'
require 'securerandom'
require 'minitest/autorun'
require 'minitest/spec'
require 'minitest/reporters'
require 'zerozero_pilot'

require 'data/constants'
require 'data/urls'
require 'data/recordsets'
require 'mocks/apis'

Minitest::Reporters.use!

def create_model_for_module(test_module, class_name, url)
  identifier = "test_app.#{class_name}"

  ZeroZeroPilot::ModuleUtils.create_module_model(identifier, url, test_module, TestConstants::TOKEN)
end

def module_with_model(class_name = 'Example', url = TestUrls::EXAMPLE_LIST, test_key = SecureRandom.uuid.gsub('-', ''))
  # using test key to ensure module is unique per test
  new_module = ZeroZeroPilot::ModuleUtils.constantize_module("ModelSpec#{test_key}")

  create_model_for_module(new_module, class_name, url)

  new_module
end
